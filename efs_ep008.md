# EFS ep8 : Sinusoïdes et impédances complexes

**Épisode 8 ( 15 juillet) 2022 )** 

<img src="images/efs_008/01_chat_tequila.png" alt="chat" width="300"/>

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=RqY8gKRqllw
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1532596733

# Previously in EFS
[![youtube](images/youtube.png) (00:05:57)](https://www.youtube.com/watch?v=RqY8gKRqllw&t=5m57s)  

On s'intéresse à une gamme de signaux particulier : les sinusoïdes.

Elles s'écrivent sous la forme $`U(t) = A \cos (\omega t + \phi)`$

## trigonométrie
On rapelle que, dans un triangle rectangle :  
![](images/efs_008/02_triangle.png)  
- $`\cos(\theta) = \frac {coté \ adjacent} {hypoténuse}  `$
- $`\sin(\theta) = \frac {coté \ opposé} {hypoténuse}  `$


On peut retrouver ce triangle rectangle dans le cercle unitaire (dont le rayon vaut 1)  
![](images/efs_008/03_cercle_unitaire.png)


- $`\cos(0)=1 `$
- $`\cos(\frac \pi 2)=0 `$
- $`\cos(\pi)=-1 `$
- $`\cos(\frac {3\pi} 2)=0 `$


![](images/efs_008/04_sinusoide.png)

## Analyse de Fourier
Fourier nous a appris que tout signal périodique peut être décomposé comme une somme (possiblement infinie) de sinusoïdes.  
Ce qui permet d'étudier la réponse des schémas électroniques à des sinusoïdes pour en extrapoler la réponse à un signal plus complexe.

## sinusoïdes
 $`U(t) = A \cos (\omega t + \phi)`$  
 Avec Les 3 caractéristiques définissant un signal sinusoïdal: 
- A : l'amplitude du signal. L'unité dépend de ce qu'on est en train d'observer : Volts si on regarde une tension, ampères pour une intensité, etc...
- $`\omega`$ : la pulsation en rad/s
- $`\phi`$ : la phase à l'origine en radians (rad)

On s'est rendu compte que la plupart des filtres électroniques ne touchaient pas à la pulsation.  
- Ils peuvent retarder une sinusoïde : la phase est modifiée. Par exemple avec le condensateur ou l'intensité est en avance par rapport à la tension.  
- Ils peuventt modifier l'amplitude : amplifier ou atténuer le signal.  
- Mais en général ils ne touchent pas à la pulsation (ou la fréquence)  

Donc pour coder la sinusoïde, on va chercher à représenter le couple A et $`\phi`$. A étant une "grandeur" (Volts, ampères, etc) et $`\phi`$ étant un angle (en rad).  
Les mathématiciens savent qu'une bonne façon de représenter ça passe par les nombres complexes...


# Les nombres complexes 
## définition
[![youtube](images/youtube.png) (00:24:57)](https://www.youtube.com/watch?v=RqY8gKRqllw&t=24m57s) 

Jusqu'ici on manipulait des nombres réels.  
L'espace des nombres rééels ($`\R`$) peut être représenté sur un axe horizontal de $`-\infty`$ à $`+\infty`$ avec 0 au centre.  
Cet axe contient tous les nombres réels existants.  
![](images/efs_008/05_espace_reels.png)

Mais dans cette espaces, certaines équations n'ont pas de solutions. Par exemple $`x^2=-1`$.  
- tout nombre positif au carré est positif
- tout nombre négatif au carré est positif
- 0² = 0
  ⇒ donc il n'existe aucun réél x tel que x²=-1

Les mathématiciens n'étaient pas heureux, ils ont donc imaginé un nombre i tel que i² = -1.   
Les physiciens, pour ne pas confondre avec l'intensité, on renommé ce i en j. Dans la suite on utilisera donc j tel que **j² = -1**.  

Ce nombre j n'existe pas, il n'est pas réel  (il n'appartient pas à $`\R`$), il est 'imaginaire'.  
j ne peut donc pas être représenté sur l'axe des réels, on le place donc sur un axe vertical, des nombres imaginaires "pures", contenant les "j fois quelque chose" : 1j, 2j, -j, 0j=0, -1.5j,…

Ensuite on peut définir d'autres nombres qui tapissent le "plan complexe" ($`\Complex`$) représenté par ces 2 axes : par exemple 2+j  
![](images/efs_008/06_plan_complexe.png)

  
Un nombre complexe est un nombre sous la forme $`\underbrace{x} _{partie \ réelle} + \underbrace{jy} _{partie \  imaginaire}`$  

Par exemple :
- 2+j
- 1-j
- 2+4j

**Note :**
- Les imaginaires purs font partie des complexes, leur partie réelle est juste nulle.
- Les réels font partie des complexes, leur partie imaginaire est juste nulle.


## Opérations sur les complexes
[![youtube](images/youtube.png) (00:35:08)](https://www.youtube.com/watch?v=RqY8gKRqllw&t=35m8s)  

Soit 2 complexes :  
- $`C_1 = a+bj`$  et  
- $`C_2 = a'+b'j`$  

### Addition
```math
\begin{align*}
C_1 + C_2 &= (a+bj) + (a'+b'j) 
\\
&= a + a' + bj + b'j 
\\
&= (a+a') +(b+b')j 
\end{align*}
```

Pour additioner 2 complexes, on additionne leurs parties réelles entre elles, et leurs parties imaginaires entre elles.

### Soustraction
```math
\begin{align*}
C_1 - C_2 &= (a+bj) - (a'+b'j) 
\\
&= a - a' + bj - b'j 
\\
&= (a-a') +(b-b')j 
\end{align*}
```

Pour soustraire 2 complexes, on soustrait leurs parties réelles entre elles, et leurs parties imaginaires entre elles.

### Multiplication
```math
\begin{align*}
C_1 \times C_2 &= (a+bj) \times (a'+b'j) 
\\
&= aa' + ab'j + a'bj + bb'j²
\\
&= (aa' - bb') + (ab'+a'b)j
\end{align*}
```

Pour la multiplication, la méthode est moins facile a retenir. Il y a une façon de faire plus simple.

Un nombre complexe peut être écrit sous forme algébrique (bataille navale): $`a+bj`$  
Mais il existe aussi la forme géométrique : $`\rho e ^{j\theta}`$.  
- $`\rho`$ est le module : la distance du centre au point.
- $`\theta`$ est l'argument : l'angle en radians entre l'axe $`\R`$ et la droite passant par le centre et le point.
  
![](images/efs_008/07_complexe_geometrique.png)

Sous forme géométrique, C₁ et C₂ peuvent s'écrire : 
- $`C_1 = \rho e ^{j\theta}`$.
- $`C_2 = \rho' e ^{j\theta'}`$.

```math
\begin{align*}
C_1 \times C_2 &= \rho e ^{j\theta} \times \rho' e ^{j\theta'} 
\\
&= \rho \rho' e ^{j\theta} \ e ^{j\theta'} 
\\
&= \rho \rho' e ^{j(\theta + \theta')} 
\end{align*}
```

Pour multiplier 2 complexes sous forme géométrique, on multiplie leurs modules, et on ajoute leurs arguments. C'est beaucoup plus simple que sous forme algébrique


## Et nos sinusoïdes ?
La notation géométrique des complexes fait intervenir un angle (l'argument) et une grandeur (le module). Un angle et une grandeur... On avait vu que c'est ce qui caractérisait nos sinusoïdes !

À la sinusoïde $`A \cos (\omega t + \phi)`$ on va faire correspondre la forme géométrique complexe $`A e^{j\phi}`$

Si on revient à notre condensateur :  
On ne va plus considérer la tension en fonction du temps $` U_c(t) = U \cos (\omega t + \phi)`$ mais la tension complexe $` \underline{U_c} = Ue^{j\phi} `$  
On pourra faire de même avec l'intensité i qui se mettra sous la forme de i complexe : $`\underline{i} = Ie^{j\phi '}`$

# Impédance 
## L'impédance du condensateur
La loi d'ohm complexe peut s'appliquer : $`\underline{i} =  \frac {\underline{U_c}}{\underline{Z}}`$

Pour le condensateur, $`\underline{Z} = \frac {1}{jC\omega}`$  
L'impédance indique bien que le condensateur modifie la valeur d'amplitude mais aussi la déphasage entre tension et courant.  
C'est une sorte de résistance qui joue sur l'amplitude mais aussi sur le "retard" : la phase.

![](images/efs_008/08_condensateur_complexe.png)

$`\underline{Z} = \frac {1}{jC\omega} = \frac {j}{j^2C\omega} = -\frac {j}{C\omega}`$

Dans le plan complexe, on voit que c'est un imaginaire pur négatif, d'où un déphasage de $`- \frac \pi 2`$

donc 
$`\underline{Z} = \frac 1 {C\omega} e ^ {-j \frac \pi 2}`$

![](images/efs_008/09_impedance_complexe.png)





Plaçons une tension $`U=A \cos (\omega t) `$ (on considère aucun déphasage à l'origine).  
Ça correspond au nombre complexe $`\underline{U} = A e ^{j.0} `$  
On cherche l'intensité qui circule : appliquons la loi d'ohm aux complexes : 

```math
\begin{align*}
\underline{i} &= \frac  {\underline{U}} {\underline{R}} 

\\ \ \\

\underline{i} & = \frac {A e ^{j.0} } {\frac 1 {C\omega} e ^ {-j \frac \pi 2}}

\\ \ \\

\Big\| \ \ \ \ \underline{i} & = AC\omega e ^{j \frac {\pi}{2}}
\end{align*}
```


On utilise l'impédance exactement comme une résistance, sauf que c'est un complexe et pas un réel.


## Synthèse des impédances
[![youtube](images/youtube.png) (00:55:26)](https://www.youtube.com/watch?v=RqY8gKRqllw&t=55m26s) 

![](images/efs_008/10_synthese.png)

### Résistance
$`\underline{Z} = R`$  
Pas de partie imaginaire → la résistance n'introduit aucun déphasage entre courant et tension.

### Condensateur
$`\underline{Z} = \frac {1}{jC\omega} = -\frac {j}{C\omega}`$  
C'est un imaginaire pur (négatif), il y a donc un déphasage de $`-\frac \pi 2`$  entre le courant et la tension. Pour un condensateur, le courant est en avance de $`\frac \pi 2`$

### Inductance
$`\underline{Z} = {jL\omega}  `$
Nous étudierons ce composant plus tard, mais on remarque ici qu'on a aussi un imaginaire pur mais positif cette fois.  
Le déphasage introduit pas la bobine sera donc de $`+\frac \pi 2`$ et le courant sera alors en retard sur la tension.  


### Globalement 
$`\underline{Z} = \underbrace{X} _{perte\ d'énergie} + \underbrace{jY}_{stockage\ d'énergie}  `$  
La partie réelle de l'impédance est le coté "résistif", qui dissipe l'énergie en chaleur.  
La partie imaginaire correspond à un stockage d'énergie, énergie qui peut être "rendue" (il n'y a pas de pertes) . On parle de capacitance ou d'inductance selon le signe de la partie imaginaire.

- La résistance dissipe l'énergie par effet Joule (pertes)
- Le condensateur stocke l'énergie grâce à un champ électrique
- La bobine stocke l'énergie grâce à un champ magnétique.





# Récapitulons
[![youtube](images/youtube.png) (01:07:26)](https://www.youtube.com/watch?v=RqY8gKRqllw&t=1h7m26s) 

![](images/efs_008/11_recapitulons.png)

Les sinusoïdes s'écrivent sous la forme :  
$`A \cos (\omega t + \phi)`$

Et à chaque sinusoide, on fait correspondre un nombre complexe :  
$`A e ^{j\phi}`$  
qui est le nombre, dans le plan complexe, à la distance A et à l'angle ϕ

On peut ainsi définir l'équivalent d'une résistance en nombre complexe, l'impédance :  
$`
\underline{Z} = \frac {\underline{U}} {\underline{i}}
`$

Z va donc nous permettre de comprendre :
- Le rapport entre l'amplitude de la tension et l'amplitude de l'intensité
- La différence de phase entre la tension et l'intensité.


Ensuite ce principe fonctionne partout…

## Un exemple
### circuit RC "pont diviseur"
Prenons un circuit RC  alimenté avec une tension Ue.  
On cherche ce qu'on récupère en sortie : Us = tension aux bornes du condensateur. 
![](images/efs_008/12_filtre_rc.png)

Ceci a déjà été vu : c'est un pont diviseur un peu particulier ou R₂ a été remplacé par un condensateur !

La formule des ponts diviseurs c'est :
```math
U_S = \frac {R_2}{R_1+R_2}U_E
```
### En complexe

```math
\begin{align*}
  \underline{Us} &= \frac {\underline{Z_C}}{\underline{Z_R}+\underline{Z_C}} \ \underline{U_E}
  \\\ \\
  \underline{Us} &= \frac {\frac{1}{JC\omega}}{R +\frac{1}{jC\omega}} \ \underline{U_E} 
  \\\ \\
  \underline{Us} &= \frac {1}{RjC\omega +1} \ \underline{U_E}
  \\\ \\
  \Big\| \ \ \ \underline{Us} &= \frac {1}{1+RjC\omega} \ \underline{U_E}
\end{align*}
```

### Aux limites
#### Si ω tend vers l'infini
En prenant une très grande fréquence, ω est très grand, or 1 divisé par "très grand", ça tend vers 0.  
Donc pour de très grandes fréquences, Us tend vers 0.

#### Si ω tend vers zéro
Par contre pour les très basse fréquences, ω est très petit, proche de 0. 1/(1+0),   tend vers 1.  
Donc pour de très basse fréquence ou pour du continu, Us=Ue.

#### conclusion
On a affaire à un **filtre "passe bas"**, il laisse passer les basses fréquences, et atténue fortement les hautes fréquences.


### Une autre façon de voir les choses


ZR = R , quelque soit la fréquence ça ne bouge pas.  
Par contre 
```math
Z_C = \frac{1} {jC\omega}
```
- Pour un courant continue, f=0 donc Zc tend vers l'infini. Le condensateur se comporte comme un circuit ouvert, comme si il n'y avait rien : Us = Ue

- Pour une pulsation tendant vers l'infini, Zc tend vers 0, le condensateur se comporte comme un fil : Us = 0

Le condensateur n'intervient pas pour les basses fréquences, mais "tue" les hautes fréquences en les court-circuitant.


## Vers les diagrammes fréquentiels
[![youtube](images/youtube.png) (01:17:06)](https://www.youtube.com/watch?v=RqY8gKRqllw&t=1h17m6s)

Le rapport Us/Ue du circuit ci-dessus est proche de 1 sur les basses fréquences et proche de 0 vers les hautes fréquences, on obtient des diagrammes du genre de ci-dessous. 


![](images/efs_008/13_passe_bas.png)

Nous les étudierons en détail plus tard.

## Question : Mais d'ou vient ce #$%§ de "e" ?
[![youtube](images/youtube.png) (01:21:03)](https://www.youtube.com/watch?v=RqY8gKRqllw&t=1h21m03s)


La formule d'Euler [![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/Formule_d'Euler) nous dit que :  
```math
e^{j\theta} = \cos \theta + j \sin \theta
```
Or cosinus et sinus varient entre -1 et 1

On multiplie par le module $`\rho`$ pour pouvoir avoir n'importe quelle valeur :
```math  
\rho e^{j\theta} = \rho \cos \theta + j \rho \sin \theta
```
 
On obtient donc une équivalence entre :  
```math
A + B j  =  \rho e^{j\theta} 
```  
avec :
```math
A = \rho \cos \theta \\
B = \rho \sin \theta
```
![](images/efs_008/14_euler.png)


----

![fin](images/thats_all_folks_bugs.png)

Dans le [prochain épisode](efs_ep009.md) nous continuerons sur les complexes (II le retour de la vengeance).

