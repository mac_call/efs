# EFS ep1 : Les bases

**Épisode 01 ( 27 Mai 2022 )** 

<img src="images/efs_001/01_chat_polystyrene1.png" alt="chat01" width="300"/>

- ![youtube](images/youtube.png) Youtube :
    -  https://www.youtube.com/watch?v=YFQTamaRLpg
- ![twitch](images/twitch.png) Twitch : 
    - https://www.twitch.tv/videos/1310505119
    - (+) https://www.twitch.tv/videos/1310595015



# Introduction
[![youtube](images/youtube.png)](https://www.youtube.com/watch?v=YFQTamaRLpg&t=2m36s) (00:02:36)

EFS (Électronique From Scratch) se veut un petit programme d'initiation à l'électronique.

Un choix à faire :
- pilule bleue : analogie, mensonge dès le début puis découverte du "vrai" au fur et à mesure.    
- pilule rouge : on  part de la théorie complète qu'on peut simplifier énormément au début. Rien n'est caché au départ. 

⇒  la pilule rouge évite de déconstruire ensuite les choses cachées du début. C'est le principe qui a été choisi pour ce programme :) 
Les équations seront montrées, commentées et vulgarisées, mais pas résolues. 

Don't panic !

# La découverte de l'électricité 
  [![youtube](images/youtube.png)](https://www.youtube.com/watch?v=YFQTamaRLpg&t=9m0s) (00:09:00)

## L'électricité dans la nature 
- Orage, foudre, arc électrique
- Tribo-électricité
    - Plus connue sous la désignation "électricité statique"
    - "tribo" vient du grec et signife "frotter"
    - "électricité" / "électron" vient du mot "ambre" en grec

   ![Effet de la tribo-electricité sur un objet de type félin](images/efs_001/02_chat_polystyrene2.png)  

## Les charges électriques
  [![youtube](images/youtube.png) (00:10:26)](https://www.youtube.com/watch?v=YFQTamaRLpg&t=10m26s)

La création d'électricité par frottement amène la notion de création de "charges".  
On peut, par exemple, frotter un morceau de PVC (règle) avec un chiffon en laine.
Des "charges" s'échangent entre le chiffon et la règle, et vont créer un déséquilibre. Le morceau de plastique est ensuite capable de "soulever" des petits morceaux de polystyrène expansé : les mouvement des charges a créé une force d'attraction sur le polystyrène.

_**Question sur les ions** : 
[![youtube](images/youtube.png) (00:12:32)](https://www.youtube.com/watch?v=YFQTamaRLpg&t=12m32s)  
Les "charges" ne sont pas des "ions".  Un ion est une molécule (plusieurs atomes) chargée (donc non neutre). On ne trouve les ions que dans les liquides ou le plasma globalement. Sur des solides, les atomes ne peuvent pas bouger, mais dans certains matériaux, certains électrons (dit "libres") peuvent migrer d'un atome à l'autre._


Très tôt on a postulé l'existence de 2 types de charges : positives et négatives.
- 2 charges de même signe se repoussent
- 2 charges de signes différents s'attirent

Les  interactions entre charges sont à la base de notre univers et sont des forces importantes.

## "Force électrique" et parallèle avec la gravité
[![youtube](images/youtube.png) (00:17:50)](https://www.youtube.com/watch?v=YFQTamaRLpg&t=17m50s)  

Quelle est cette force ? 
La physique nous dit : 
```math
\vec{F}=\underbrace{ \frac{1}{4 \pi \mathcal{E}_0} }_{constante}   \frac{q_1q_2}{r^2}  \vec{u}
```
avec :
- q₁, q₂ : la charge en Coulomb (C) de chaque particule
- r :  la distance en mètres (m) entre les 2 particules
- ε₀ : la permitivité du vide ( une constante universelle [![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/Permittivit%C3%A9_du_vide) )
- u : le vecteur unité

![Force électrique](images/efs_001/03_force_elec.png)


les physiciens ont remarqué la similarité avec la gravité où 2 masses m₁ et m₂ s'attirent avec une force :
```math
\vec{F}= \mathcal{G}  \frac{m_1m_2}{r^2}  \vec{u}
```
avec :
- m₁, m₂ : la masse en kg de chaque objet
- r :  la distance en mètres (m) entre les 2 objets
- 𝓖 : la constante gravitationelle ( une constante de l'univers  [![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/Constante_gravitationnelle) )
- u : le vecteur unité
![](images/efs_001/04_force_gravite.png)

Les deux forces s'atténuent très vite quand les masses (ou les  charges) s'éloignent ( division par r² ) et augmentent par le produit de 2 quantités (kg pour m₁ m₂ ou  C pour les charges q₁ et q₂)


##  Champ de gravité et champ électrique
[![youtube](images/youtube.png) (00:22:31)](https://www.youtube.com/watch?v=YFQTamaRLpg&t=22m31s) 

Posons une planète de masse m₁ dans l'espace.  
On peut définir, en chaque point de l'espace, la force subie par un corp de masse m subissant une certaine attraction de la planète m₁.  
Les différentes forces forment ainsi un "champ de gravité" de la planète m₁.  
![](images/efs_001/05_champ_elec.png)

```math
\vec{G}  = \mathcal{G} {\frac{m_1}{r^2}}\vec{m}     
```

Ce champ est conservatif : La force ne dépend pas de l'état du système mais uniquement de ses propriétés. On peut tracer un chemin dans l'espace revenant à son point de départ, et donc revenant à la même valeur de force. 
Dans le cas d'un champ conservatif, on peut définir un "potentiel" en chaque point, et le champ se calcul par rapport à ce potentiel.

[![youtube](images/youtube.png) (00:26:25)](https://www.youtube.com/watch?v=YFQTamaRLpg&t=26m25s)  
Quand on fait tomber un objet, l'objet au départ est à une certaine altitude et une vitesse nulle.  
![](images/efs_001/06_energie.png)  
À la fin il a une vitesse max et une altitude nulle.  

On a tranformé une position en vitesse, une énergie potentielle en énergie cinétique :
```math
E_c=\frac{1}{2}mv^2
```

  
Avec cette similarité entre gravité et électricité,  on peut donc définir une "énergie potentielle" pour le champs électrique  

  
### Et le magnétimsme dans tout ça ?
[![youtube](images/youtube.png) (30min 06s)](https://www.youtube.com/watch?v=YFQTamaRLpg&t=30m06s)

Des particules statiques chargées électriquement forment un champ électrique qui va du + vers le -, par convention.
  
Il y a une certaine similitude avec les aimants où on sait que les pôles opposés s'attirent.
  
Pourtant si on met une particule statique (immobile) à coté d'un aimant, lui aussi immobile, il ne se passe rien.  
![](images/efs_001/07_aimant_statique.png)  
L'aimant n'a donc pas l'air d'interagir avec les particules chargées.

**MAIS**

Quand l'aimant se déplace (ou les particules se déplacent), il apparait d'autres forces. 

Il y a **2 champs**. à la fois différents et couplés.

![](images/dont_panic.png)



# Les équations de Maxwell  
 [![youtube](images/youtube.png) (33min 14s)](https://www.youtube.com/watch?v=YFQTamaRLpg&t=33m14s)
## Équations du champ électrique
### Maxwell-Gauss
```math 
\vec{\nabla}.\vec{E} = \frac{\rho}{\mathcal E_0} 
```

La divergence du champs électrique dépend de la charge électrique ρ 
 
Si on prend une petite boite, dans l'espace, contenant une charge, le champ va : soit rentrer, soit sortir, selon le signe de la charge.  
![](images/efs_001/08_divergence_electrique.png)
 
 Si on pose une charge dans l'espace, le champ E est radial (soit il rentre (-), soit il part (+), de la charge )  
![](images/efs_001/09_divergence_electrique2.png)
 
### Maxwell-Faraday
```math 
\vec{\nabla} \times \vec{E} = \dfrac{\partial \vec B}{\partial t}
```
 
 Quand un champ magnétique varie dans le temps (dérivée de B), alors ça influence le champ électrique (rotationnel de E à gauche).
 
 ## Équations du champ magnétique
 ### Maxwell-Thompson
```math 
\vec{\nabla} . \vec{B} = 0
``` 
Si on prend une petite boite, dans l'espace, le flux magnétique entrant doit être égal au flux sortant.  
![](images/efs_001/10_divergence_magnetique.png) 

Il n'y a pas de "charge magnétique" contrairement à la charge électrique.  

### Maxwell-Ampère
```math 
\vec{\nabla} \times \vec{B} = \mu_0  \vec j + \mu_0 \mathcal E_0  \dfrac{\partial \vec E}{\partial t}
```
 
Pour avoir un  champ magnétique, il faut soit :
- un courant électrique (un déplacement de charges), c'est le terme avec j
- un champ électrique variant ( teme avec la dérivée de E )

**Notes :**
Dans les membres de gauche des équations de Maxwell : 
-  .  : désigne un produit scalaire (le résultat est un entier) 
- x :  désigne un produit vectoriel (le résultat est un vecteur)
- ∇ : désigne les dérivées partielles dans les 3 dimensions x, y et z 
 
- La divergence de E ([![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/Divergence_(analyse_vectorielle))) :
```math 
\vec{\nabla}.\vec{E} = 
  \begin{pmatrix}  
    \dfrac{\partial }{\partial x}  \\ \\
    \dfrac{\partial }{\partial y}  \\ \\
    \dfrac{\partial }{\partial z} 
  \end{pmatrix}
.
\begin{pmatrix}  
  e_x \\ \\ \\
  e_y  \\ \\ \\
  e_z
\end{pmatrix}
=
\dfrac{\partial }{\partial x}  . \vec e_x +
\dfrac{\partial }{\partial y}  .  \vec e_y +
\dfrac{\partial }{\partial z} .  \vec  e_z
``` 
divergence  = différence de ce qui rentre et ce qui sort entre la gauche et la droite, le devant et le derrière, le haut et le bas, en tout point de l'espace. 

- Le rotationnel du champ E  ([![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/Rotationnel)) : 
```math
\vec{\nabla} \times \vec{E}
```
rotationel = de combien tourne le champ (dans les 3 dimensions)



## Conclusion de ce que dit Maxwell
[![youtube](images/youtube.png) (00:39:12)](https://www.youtube.com/watch?v=YFQTamaRLpg&t=39m12s)

Pour créer un champ électrique il faut soit :
- une charge électrique
- un champ magnétique qui varie dans le temps (par exemple en déplaçant un aimant)
  

Pour créer un champ magnétique il faut soit : 
- un aimant
- des charges qui se déplacent (c'est a dire un courant électrique)
- un champ électrique qui varie dans le temps 

Le champs électrique et le champ magnétique sont 2 champs couplés dont l'un dépend des variations temporelles de l'autre (et vice versa)

![](images/efs_001/11_interaction_E_B.png)
 
## Appartés 
### questions sur les termes de gauches des équations de Maxwell: le "triangle inversé" c'est quoi ?
[![youtube](images/youtube.png) (00:40:21)](https://www.youtube.com/watch?v=YFQTamaRLpg&t=40m21s)
   
   ### question "c'est quoi une dérivée ?" 
[![youtube](images/youtube.png) (00:43:10)](https://www.youtube.com/watch?v=YFQTamaRLpg&t=43m10s)
  

## Simplifions Maxwell pour le début !
[![youtube](images/youtube.png) (00:44:37)](https://www.youtube.com/watch?v=YFQTamaRLpg&t=44m37s)
 
**Hypothèse** : les choses ne vont pas trop vite.

Le champ magnétique peut être supposé ne variant pas "trop", on peut donc, pour le début,  dire que dB/dt = 0 ce qui perrmet de ne garder que la première équation (Maxwell-Gauss).   
On est dans le cas de l'électricité "simple" ce qui est une bonne approximation en dessous du Ghz ou du Mhz. 

Quand la longueur d'onde commence à se rapprocher de la taille des pistes du circuit imprimés, il faudra revenir sur ces simplifications. On passera alors du modèle de "fil électrique" simple, à celui de "lignes de transmission" faisant apparaitre des impédances complexes.  
 
 
 
# Les notions associées aux circuits électriques 
[![youtube](images/youtube.png) (00:53:26)](https://www.youtube.com/watch?v=YFQTamaRLpg&t=53m26s)
 
Dans un circuit électrique, on a :
- un générateur, par exemple une pile (à gauche)
-  des conducteurs (supposés parfait) qui ne s'opposent pas du tout au courant. (en rouge, vert et bleu)
-  des composants (ici des dipôles à 2 pattes) : une résistance et une diode  
![](images/efs_001/12_circuit1.png)
 
 
## L'intensité 
- L'intensité est notée I
- Sur un circuit, elle est symbolisée par une flèche posée sur un conducteur
- Elle mesure le courant électrique
- C'est le nombre de charges par secondes qui circulent dans le fil.
- l'intensité sera la même partout dans le circuit ci-dessus parce qu'il n'a qu'une seule boucle.
- l'intensité se mesure en Ampère (A), qui sont des Coulombs par secondes
- C'est le "débit du tuyau d'eau"
-  ![](images/efs_001/13_circuit1_i.png)


## Potentiels, tension et différence de potentiels

Les charges qui se déplacent cherchent à combler un déséquilibre. Elles vont de là ou il y en a trop vers là ou il n'y en a pas assez, "emmenées" par le champ électrique.  

En tout point du circuit existe un potentiel.

L'intensité va des hauts potentiels vers les bas potentiels pour essayer de rééquilibrer les choses (comme l'eau qui s'écoule vers la mer). 

L'unité de mesure des potentiels est le Volt (V).

Une différence de potentiels (ou tension) se mesure entre 2 points du circuits. 

En pratique on mesure pratiquement toujours une différence de potentiel entre 2 points, rarement un potentiel seul. le plus souvent on utilise la terre comme référence à 0v mais ce n'est pas obligatoire.  
Le point de référence est arbitraire.

- La tension se note par une flèche à coté du circuit, entre les 2 points ou on mesure la ddp
- elle est notée U
- elle s'exprime en Volts (V)
- dans un conducteur (parfait), la tension est toujours de 0V

![](images/efs_001/14_circuit2.png)

## Loi des nœuds

Sur un nœud du circuit, la somme des courants entrant est égale la somme des courants qui en sortent.  
ci-dessous i₀ + i₁ = i₂ + i₃

![](images/efs_001/15_loi_noeuds.png)

## Loi des mailles

Dans une boucle de circuit, la somme des tensions est nulle.  
Ci-dessous :  U₁ + U₂ + U₃ + U₄ = 0  
![](images/efs_001/16_loi_mailles.png)


## Convention récepteur

Le sens de notation des tensions et du courant dans un composant non générateur sont par défaut en "convention récepteur" : le sens du courant fleché à l'inverse du sens de la flèche de tension.  
![](images/efs_001/17_convention_recepteur.png)


## Exemple
### Exercice sur papier
![](images/efs_001/18_circuit3_.png)

### Exemple "en vrai"
[![youtube](images/youtube.png) (01:34:18)](https://www.youtube.com/watch?v=YFQTamaRLpg&t=5658s)



---

![fin](images/thats_all_folks_bugs.png)

Dans le [prochain épisode](efs_ep002.md) nous  parlerons des résistances.
 
