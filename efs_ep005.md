# EFS ep5 : Le condensateur

**Épisode 5 ( 24 juin 2022 )** 

<img src="images/efs_005/01_chat_sandwich.png" alt="chat" width="300"/>

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=rGTnc2iRDaU
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1512865765

# Introduction
[![youtube](images/youtube.png) (00:09:46)](https://www.youtube.com/watch?v=rGTnc2iRDaU&t=9m46s)  

Le condensateur est un dipôle.  
C'est un sandwich, il est composé de deux "tartines" conductrices (les armatures) séparées par un isolant (le diélectrique). 

![photo condensateur ](images/efs_005/02_condensateur.png)
![condensateur est un sandwich](images/efs_005/03_condensateur_sandwich.png)

C'est souvent le composant qui lâche en premier dans les circuits électronique. Les condensateurs peuvent gonfler, couler, voire exploser avec le temps. Ils vieillissent mal. Ils peuvent aussi être de mauvaise qualité et choisis un peu trop "limite" par rapport aux besoins. 


# Principe 
[![youtube](images/youtube.png) (00:13:35)](https://www.youtube.com/watch?v=rGTnc2iRDaU&t=13m35s)  

Un condensateur vide et non branché est équilibré électriquement : il y a autant de charges positives et négatives sur chacune des armatures.

Si on commence à faire passer un courant de charges vers le condensateur, une des plaque va comporter plus de charges positives, et l'autre aura plus de charges négatives (en fait une des plaques aura un déficit d'électrons et l'autre un surplus d'électrons).   
Il va se produire un déséquilibre de charges, et donc un champ électrique E  va se mettre en place entre les plaques du condensateur. Qui dit champ électrique dit différence de potentiel ou tension électrique.  


![condensateur principe](images/efs_005/04_condensateur_schema.png)



Le courant ne passe pas le diélectrique, et le champ, donc la tension reste présente même quand on enlève l'alimentation qui a apporté les charges. Théoriquement la tension reste au borne du condensateur chargé pour un temps infini, en pratique le condensateur va se vider tout seul au bout d'un temps plus ou moins long.  

**Attention aux condensataurs chargés, même dans les circuits hors tension !**



# Caractérisation
[![youtube](images/youtube.png) (00:21:59)](https://www.youtube.com/watch?v=rGTnc2iRDaU&t=21m59s)  

Une charge plus ou moins grande peut s'accumuler, en fonction de la "capacité" du condenstateur. la capacité s'exprime en Farad (F).  
La capacité d'un condenstateur dépend de :
- la surface des armatures 
- la distance entre les armatures
- la "qualité" du diélectrique (l'isolant entre les plaques)
```math
C = \mathcal{E_0} \mathcal{E_r} \frac A d
```
avec :
- $`\mathcal{E_0}`$ : la permitivité du vide (constante universelle) ($`F.m^{-1}`$)
- $`\mathcal{E_r}`$ : permitivité relative du diélectrique 
- A : la surface des armatures (m²)
- d : la distance entre les armatures (m)


Les caractéristiques importante d'un condensateur sont :
- sa capacité en Farad (F). 1F étant une capacité énorme et peu commune, on utilise en général les :
    - μF → $`10^{-6} `$ F
    - nF → $`10^{-9} `$ F
    - pF → $`10^{-12} `$ F
- sa tension maximale d'utilisation (V) . Un condensateur réel à une tension "limite" au delà de laquelle la ddp est trop importante et le diéléctrique "claque" et se détruit.

**Calcul de surface d'un condensateur de 1F**  
Soit (source wikipedia): 
- $`\mathcal{E_0} =  8.85418782.10^{−12} F.m^{−1}`$ 
- $`\mathcal{E_r} =  6 `$ : la permitivité relative du mica
- C = 1F  : la capacité de notre condensateur hypothétique
- d = 0.1mm = $`10^{-4}m `$ : la distance entre les 2 armatures de notre gros condo
```math
\begin{align*}
C &= \mathcal{E_0} \mathcal{E_r} \frac A d \\
\Leftrightarrow A &= \frac {d \times C } {\mathcal{E_0} \mathcal{E_r}}
\end{align*}
```
Numériquement on obtient environ **1.9 millions de m²** pour la surface des armatures d'un consensateur de 1 Farad dont le diélectrique est du mica de 0.1mm d'épaisseur !



_**Aparté sur la bouteille de Leyde**  [![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/Bouteille_de_Leyde)_

_[![youtube](images/youtube.png) (00:29:26)](https://www.youtube.com/watch?v=rGTnc2iRDaU&t=29m26s)_

_La bouteille de Leyde est un condensateur naturel formé par une bouteille en verre (le diélectrique) revêtue d'une couche conductrice à l'extérieur, et remplie avec des feuilles d'étain chiffonnées reliées à une électrode passant par le bouchon isolant.  
Elle fut inventée en 1745.  
Une fois ce condensateur chargé, la personne qui touchait la bouteille et l'électrode recevait un choc électrique. C'était une "curiosité savante" à l'époque._


# La charge/décharge du condensateur
## Visualisation à l'oscillo

[![youtube](images/youtube.png) (00:28:30)](https://www.youtube.com/watch?v=rGTnc2iRDaU&t=28m30s)


![circuit de charge](images/efs_005/05_charge_condo.png)  
Une source de tension (ici 10V) branchée à un condensateur via une résistance, permet de  "charger" un condensateur. 

En branchant un oscilloscope aux bornes du condensateur (comme un voltmètre) on peut mesurer la tension dynamiquement, et la voir évoluer.  
Ci-dessous on est parti de 0v (la petite flêche jaune en bas à gauche sur l'écran de l'oscillo) et en quelques secondes, la ligne jaune a atteint les 10 V (2V/div verticales sur l'oscillo x 5 divisions "montées")  
![circuit de charge](images/efs_005/06_condo_charge.png)

Si on débranche l'alimentation (fil violet) la tension aux bornes du condensateur reste à 10V. Le condensateur agit comme une micro-batterie, un mini réservoir d'électricité.  
Le condensateur n'étant pas parfait, au bout de quelques minutes, il va tout de  même finir par se décharger tout seul.

Si on connecte le fil violet de l'autre coté du circuit,  le condenstaur va se décharger dans la résistance de droite et la led va briller quelques secondes.

Le condensateur accumule une charge et le stockage se fait sous forme de champ électrique entre les armatures.  
On verra plus tard que les bobines stockent, elles, sous forme de champ magnétique.


## Les équations
[![youtube](images/youtube.png) (00:55:15)](https://www.youtube.com/watch?v=rGTnc2iRDaU&t=55m15s) 

### La charge du condensateur

On applique une tension E a un circuit composé d'une résistance et d'un condensateur en série.  
![](images/efs_005/07_equation_charge.png)

Les physiciens nous disent : 
$`q = C \times U_c`$

avec 
- $`q`$ : la charge sur l'armature du condensateur (en C)
- $`C`$ : la capacité du condensateur (en F)
- $`Uc`$ : la tension aux bornes du condensateur (en V)

Si on dérive l'ensemble par rapport au temps t : 

```math
\underbrace{ 
    \frac {dq} {dt}
}_{I} = C \times \frac {dU_c}{dt}
```

$`\frac {dq} {dt}`$ est la dérivée de la charge par rapport au temp, autrement dit la variation instantanée de la charge dans le temp.  
On peut parler de charge par seconde, de débit de charge, c'est l'intensité (I).

D'après la loi des mailles :
```math
\begin{align*}
    E &= U_c + U_r \\
    E &= U_c + R.I \\
    E &= U_c + R C \frac {dU_c} {dt}
\end{align*}
```

On obtient une "équation différentielle" : on y  trouve à la fois Uc et la dérivée de Uc par rapport au temps.   
C'est une équation dont l'inconnue est une fonction : on cherche la fonction Uc qui fait que l'équation est respectée.

Le mathématicien sait que la solution d'une telle équation différentielle est de la forme :
```math
U_c = \lambda e^{-\frac 1 {RC} t} + \mu
```

Avec λ et μ des constantes à déterminer.

Or on sait que, au départ, le condensateur est vide, et à la fin, il est "plein".  
Donc : 
- à t = 0 :  q=0 → Uc=0 ⇒ λ + μ = 0 → λ = -μ 
- à t = ∞ :  Uc = E → E = 0 + μ  → **μ = E**  
  ⇒ **λ = -E**

On obtient au final :
```math
U_c = E(1- e^{- \frac 1 {RC} t})
```

Ce qui donne une courbe ayant cette allure :

![](images/efs_005/08_courbe_charge_C.png)

La tension augmente rapidement puis de moins en moins avant de tendre vers E.

On remarque, dans l'équation, la valeur 'RC' qui est la constante de temps du circuit (notée parfois τ).

Cette valeur est "spéciale" : la droite tangente à la courbe de charge, en 0, croise  E au bout de RC secondes.  
Même si ceci n'a pas trop d'intérêy pratique en tant que tel, en général, on considère que le condensateur est chargé à 90% au bout de 3τ et à 99% au bout de 5τ .


### La décharge du condensateur

on reprend la même chose mais avec E=0 : on enlève le générateur, remplacé par un fil, et le condensateur se décharge dans la résistance. 

On obtient  :
```math
U_c = E.e^{- \frac 1 {RC} t}
```

## Montage réel avec un générateur de crénaux
[![youtube](images/youtube.png) (01:20:00)](https://www.youtube.com/watch?v=rGTnc2iRDaU&t=1h20m0s)  
 
![](images/efs_005/09_montage_charge_signal_carre.png)

On va prendre les mêmes paramètres que la simulation spice [ici](https://gitlab.com/mac_call/ngspice/-/blob/master/simulation_exemples/charge_condensateur/charge_C_simu.cir), pour vérifier que tout fonctionne comme calculé. 

- C = 1uf
- R = 100k
- ⇒ RC=0.1s

Courbe de charge à l'oscillo (en bleu). En jaune, les crénaux du générateur (4V)

![](images/efs_005/10_charge_oscillo.png)

Courbe de décharge à l'oscillo

![](images/efs_005/11_decharge_oscillo.png)

Ce qui correspond bien à la simulation :

![](images/efs_005/12_charge_simu.png)

--------

# Conclusion : et paf le condo !
[![youtube](images/youtube.png) (01:47:31)](https://www.youtube.com/watch?v=rGTnc2iRDaU&t=1h47m31s)

Les condensateurs chimiques n'aiment vraiment pas être montés à l'envers. S'en suit en général une fin de vie du composant... assez violente. (si il est bien monté à l'envers ;) )

![](images/efs_005/13_paf_condo.png)

----- 

![fin](images/thats_all_folks_bugs.png)

In the [prochain épisode](efs_ep006.md) nous continuerons avec les mystères du condensateur.
