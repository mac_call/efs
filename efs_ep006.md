# EFS ep6 : Les mystères du condo...

**Épisode 6 ( 1er juillet 2022 )** 

<img src="images/efs_006/01_chat_sandwich.png" alt="chat" width="300"/>

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=gQy1UL7T-eU
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1519461210

# Previously in EFS
[![youtube](images/youtube.png) (00:01:48)](https://www.youtube.com/watch?v=&t=1m48s)  

Un condensateur est un sandwich composé de 2 plaques (armatures) conductrices séparées par un isolant (le diélectrique).  
Il se comporte comme une cuve d'eau : Quand on le connecte à une tension, un courant électrique va  accumuler des charges (+ d'un coté, - de l'autre), avec la relation $`i = \frac {dq} {dt} = C \frac {dU}{dt} `$

![](images/efs_006/02_previously.png)

Grâce à ce courant, la tension dans le condensateur (Uc)  va rapidement augmenter au départ, avant de ralentir pour finir par tangenter E (la tension du générateur).  
Quand le condensateur est "plein", Uc=E donc Ur=0 donc i=0.

La constante de temps τ=RC indique le temps au bout duquel le condesnateur est chargé au 2/3.
au bout de 3τ il est chargé à plus de 90% et au bout de 5τ, il est chargé à plus de 99%.

![](images/efs_006/03_previously_charge.png)

Entre 0 et τ secondes, on peut considérer que la charge du condensateur est linéaire, ce qui était utilisé pour avoir un balayage régulier dans les tubes cathodiques.


Le condensateur conserve sa charge quand il est débranché. Il stocke l'énergie sous forme de champ électrique. Il y a une différence de potentiel (tension) à ses bornes.

La décharge du condensateur dans une résistance a la tête opposée de la charge, avec la même constante de temps RC caractéristique du temps de décharge.

![](images/efs_006/04_previously_decharge.png)


Tout ceci s'entend avec un condensateur parfait qui n'a aucune fuite.  
En réalité, un condensateur chargé perd très doucement sa tension. 


# Modèle d'un condensateur réél

On prend un condensateur parfait auquel on ajoute des "composants parasites".  
- une résistance "de fuite" en parallèle (très forte) par laquelle il se décharge doucement.
- une résistance en série (très faible) : l'ESR = Résistance Série Équivalente.
- une self en série : les "pattes" du condo étant de petites selfs.

![](images/efs_006/05_modele.png)

Selon le degré de précision qu'on souhaite, on prend un modèle plus ou moins complexe. Pour le moment, le condensateur "parfait" est suffisant.


# Les principales familles de condensateurs

## Condensateur électrolytique - chimique
Un patte est reliée à un film conducteur en aluminium, l'autre patte est reliée à un liquide conducteur aussi. (l'électrolyte).  
Quand on le branche, de l'oxyde d'aluminium (alumine) qui est un isolant, se dépose sur le film d'aluminium créant un condensateur.  
La réaction d'oxydo-réduction ne se fait que si le condensateur est correctement polarisé.  
Connecté dans le mauvais sens, la réaction change et un gaz est produit (qui fait explosé le condo).

Le condensateur électrolytique a l'avantage de pouvoir avoir des grosses valeurs de capacité dans un volume assez restreint.  
Par contre il a comme inconvénient d'être polarisé, de "mal vieillir" et d'avoir un forte résistance série (ESR) : il est "lent".  
Il faut le voir comme une grosse réserve d'énergie.

![](images/efs_006/06_condo_interieur.png)


## Condensateur céramique
Les condensateurs céramiques ont de bonnes capacités mécaniques (résistent bien à la chaleur et à la dilatation des éléments chauffant).


## Condensateur Tantale
Ce sont des condensateurs polarisés mais avec une ESR faible.


## Condensateur Mica, polymère...
Il existe beaucoup d'autres familles de condensateurs 

## Condensateur variable
[![youtube](images/youtube.png) (00:58:48)](https://www.youtube.com/watch?v=gQy1UL7T-eU&t=58m48s)  

Utilisés notamment dans les circuits radio. 
Plus les "peignes" sont l'un dans l'autre, plus la capacité augmente.  
![](images/efs_006/07_condo_variable.png)

[![wikipedia](images/wikipedia.png)](https://commons.wikimedia.org/wiki/File:Condensateur-variable-a-air_375x240.gif)


# Intensité vs Tension dans les composants
[![youtube](images/youtube.png) (01:02:27)](https://www.youtube.com/watch?v=gQy1UL7T-eU&t=1h2m27s)  

Le condensateur (et la bobine) ne peuvent pas être représenté par un diagramme courant-tension comme on a pu le voir précédemment.

## Générateur de tension :   
![](images/efs_006/08_gene_tension_u_i.png)

## Générateur de courant :  
![](images/efs_006/09_gene_courant_u_i.png)

## Résistance :  
![](images/efs_006/10_resistance_u_i.png)

## Diode :  
![](images/efs_006/11_diode_u_i.png)

## Condensateur :  
Pour le condensateur, i ne dépend de U mais de la variation de U :  
$`i=C \frac {dU}{dt}`$.   
On ne sait pas représenter le condensateur dans un diagramme U/I.

On a vu précédemment que le condensateur se charge puis se décharge si on lui applique un signal périodique carré suffisamment "lent".  
On va donc appliquer une tension variable (sinusoidale) de différentes fréquences (avec un générateur de fonctions).

![](images/efs_006/12_schema_etude_sinus.png)

La résistance  sera choisie assez grande et sert à mesurer i (car $`i=\frac 1 R U_R`$).  
On pourra donc voir Uc et i à l'oscilloscope

_**Attention** la mesure de Ur doit se faire avec une sonde différentielle car le 0 de cette mesure n'est pas le 0 du circuit.  
Les 2 voies de l'oscillo on leur "masse" branchées ensemble à la terre, donc sans sonde différentielle, on shunte le condensateur._


Si on commence par placer une résistance à la place du condensateur, on voit que la tension et l'intensité ont un maximum et un minimum en même temps : il n'y a pas de déphasage, ce qui est normal vu que en tout temps U=Ri :  
![](images/efs_006/13_resistance_graph_sinus_u_i.png)

tension en jaune, intensité en bleu.

Avec un condensateur comme dans le shéma ci-dessus :

![](images/efs_006/14_condo_graph_sinus_u_i.png)

La courbe de l'intensité et celle de Uc sont "décalées". L'intensité (en bleu) semble en avance sur la tension (en jaune).  
On a un déphasage.

Si on fait varier la fréquence du signal du générateur, on voit aussi que la valeur de la hauteur des courbes varie.  
Sa "résistance" semble varier en fonction de la fréquence du signal.  On parle dans ce cas **d'impédance**.


###  Un peu de math

On sait que pour les condensateurs :
```math
q = CU_c 
\\ \  \\
\frac {dq}{dt} = C \frac {dU_c} {dt}
\\ \  \\
i(t) = C \frac {dU_c} {dt}
```
Or, on applique une tension sinusoïdale :
```math
U_c(t) = A \cos(\omega t + \phi  )
```

La dérivée de $`\cos(x) = - \sin(x) = \sin(x + \pi)`$,  
et $`\sin(X) = \cos(X-\frac \pi 2)`$   
Soit la dérivée de $`\cos(x) = \cos'(x)= cos (x+\frac \pi 2)`$ 

 La suite au prochain épisode !

 # Retour sur les sondes différentielles et la masse des voies de l'oscillo
 [![youtube](images/youtube.png) (01:51:07)](https://www.youtube.com/watch?v=gQy1UL7T-eU&t=1h51m07s)  

Dans un oscilloscope, les (-) de chaque voie sont connectés ensemble, ainsi qu'a la prise de terre.  
En essayant de mesurer les tensions Ur et Uc "normalement" avec les 2 voies de l'oscillo (① et ② en rouge sur le schéma ci-dessous), on réalise donc un beau court-circuit  (liaison en noir à droite, par construction de l'oscillo, entre les broches (-) des voies).  
![](images/efs_006/15_oscillo_voies_terre.png)


C'est un problème récurrent dans les mesures à l'oscilloscope, qui se résout par l'utilisation d'une sonde différentielle.  
La sonde découple le coté mesuré de ce qui est relié à l'oscilloscope : le signal est le même mais le (-) de la sonde différentielle branchée à l'oscillo n'est PAS relié à la terre. Exit le court-circuit.


![](images/efs_006/16_sonde_diff.png)


Une autre solution pour mesurer un signal "quelconque" sur un cicuit, sans faire un court-circuit, est d'utiliser 2 voies de l'oscillo, chaque (+) étant relié de part et d'autre de la tension à mesurer (les masses étant reliées au ground). En utilisant la fonction math de l'oscillo et en soustrayant  la voie 2 de la voie 1, on obtient le signal entre les 2 sondes. 


# Conclusion : oscillo pr0n
 [![youtube](images/youtube.png) (01:45:30)](https://www.youtube.com/watch?v=gQy1UL7T-eU&t=1h45m30s)

## oscillo moderne et pas trop cher : Rigol

![](images/efs_006/17_oscillo_rigol.png)

## Semi-antiquité 
### tektronix "simple"

![](images/efs_006/18_oscillo_tektronix.png)

### Lecroy (2004-2005)

![](images/efs_006/19_oscillo_lecroy.png)


### Tektronix "haute qualité"
![](images/efs_006/20_oscillo_tektronix2.png)
 
## Antiquité : Telequipment
![](images/efs_006/21_oscillo_telequipment.png)



----- 

![fin](images/thats_all_folks_bugs.png)

Dans le [prochain épisode](efs_ep007.md) on ondulera en parlant de signaux périodiques sinusoïdaux.

