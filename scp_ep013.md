# Soirée Chill et Projets ep013 : STM32  GPIO

**Épisode 13 ( 29 septembre 2022 )** 

----
![](images/under_construction.jpg)  

---- 

<img src="images/scp_013/01_chat.png" alt="chat" width="300"/>

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1604640917
     - https://www.twitch.tv/videos/1604674037
     - https://www.twitch.tv/videos/1604765870

# Préambule

Dans [l'épisode précédent](scp_ep012.md) nous avions vu la chaîne de compilation et les logiciels necessaires pour compiler des programmes pour STM32 (blue pill), ainsi que pour dialoguer avec la carte via openOCD.

Nouas allons maintenant compiler notre premier programme, le faire fonctionner, et expliquer l'usage des différents fichiers générés ainsi que du code.

# Projet template blue pill

repo git : https://github.com/Rancunefr/template_bluepill

## structure 
- `app` : répertoire contenant notre application
    - `include` 
    - `src` : le répertoire de sources,  Le gros du programme se trouvera dans `main.c`.  
- `cmsis` :  
  - `core` : contient les librairies (.h) fournies par les constructeurs du cœur ARM. 
  - `device` : includes et .c propres a notre stm32 particulier. Ainsi que le petit code assembleur de démarrage de la carte (.s). 
- `docs` : les 3 pdfs de documentation vues dans l'épisode précédent (schéma, datasheet, manuel de ref.)
- `openocd` : le fichier de conf pour lancer openOCD avec avec ke ST-LINK V2 et la  blue pill. cette config utilise des fichiers de conf openOCD venant avec le logiciel. 
- `Makefile` : venant de cubeMX et mis au propre, pour le lancement des la compilation, etc.  
  - Il contient la définition du préfixe du compilateur à utiliser ("arm-none-eabi-"  par défaut).
  - ligne 6-13 : les fichiers source du projet : 4 fichiers ".c", 1 ".s"
    - main.c notre programme
    - fichier .it pour les interruptions
    - séparation de bsp (Board Specific Part) le code propre à la carte (allume la led), du code du micro-contrôleur (bascule PC13)


## compilation et lancement de "blink" sur la blue pill
- connexion de la bluepill au ST-LINK via les 4 fils du ports SWD (cf. [épisode SCP #12](scp_ep012.md))
- connexion duy ST-LINK à l'ordi via USB : le led rouge de la bluepill s'allume.
- on suppose le projet "template_bluepîll" récupéré de github dans `~/devel/template_bluepill` (cf. url ci-dessus)
- on se place dans le répertoire de travail : `cd ~/devel/template_bluepill`
- compilation du programme blink présent dans le projet (cf. main.c) `make` 
  - sortie :
```
╰─ make
mkdir build
arm-none-eabi-gcc -c -mcpu=cortex-m3 -mthumb   -DSTM32F103xB -Icmsis/core -Icmsis/device/include -Ibsp/include -Iapp/include -Og -Wall -fdata-sections -ffunction-sections -g -gdwarf-2 -ggdb3 -MMD -MP -MF"build/stm32f1xx_it.d" -Wa,-a,-ad,-alms=build/stm32f1xx_it.lst app/src/stm32f1xx_it.c -o build/stm32f1xx_it.o
arm-none-eabi-gcc -c -mcpu=cortex-m3 -mthumb   -DSTM32F103xB -Icmsis/core -Icmsis/device/include -Ibsp/include -Iapp/include -Og -Wall -fdata-sections -ffunction-sections -g -gdwarf-2 -ggdb3 -MMD -MP -MF"build/system_stm32f1xx.d" -Wa,-a,-ad,-alms=build/system_stm32f1xx.lst cmsis/device/src/system_stm32f1xx.c -o build/system_stm32f1xx.o
arm-none-eabi-gcc -c -mcpu=cortex-m3 -mthumb   -DSTM32F103xB -Icmsis/core -Icmsis/device/include -Ibsp/include -Iapp/include -Og -Wall -fdata-sections -ffunction-sections -g -gdwarf-2 -ggdb3 -MMD -MP -MF"build/bsp.d" -Wa,-a,-ad,-alms=build/bsp.lst bsp/src/bsp.c -o build/bsp.o
arm-none-eabi-gcc -c -mcpu=cortex-m3 -mthumb   -DSTM32F103xB -Icmsis/core -Icmsis/device/include -Ibsp/include -Iapp/include -Og -Wall -fdata-sections -ffunction-sections -g -gdwarf-2 -ggdb3 -MMD -MP -MF"build/main.d" -Wa,-a,-ad,-alms=build/main.lst app/src/main.c -o build/main.o
arm-none-eabi-gcc -x assembler-with-cpp -c -mcpu=cortex-m3 -mthumb   -DSTM32F103xB -Icmsis/core -Icmsis/device/include -Ibsp/include -Iapp/include -Og -Wall -fdata-sections -ffunction-sections -g -gdwarf-2 -ggdb3 -MMD -MP -MF"build/startup_stm32f103xb.d" cmsis/device/src/startup_stm32f103xb.s -o build/startup_stm32f103xb.o
arm-none-eabi-gcc build/stm32f1xx_it.o build/system_stm32f1xx.o build/bsp.o build/main.o build/startup_stm32f103xb.o -mcpu=cortex-m3 -mthumb   -specs=nano.specs -TSTM32F103C8Tx_FLASH.ld  -lc -lm -lnosys  -Wl,-Map=build/cubeide.map,--cref -Wl,--gc-sections -o build/cubeide.elf
arm-none-eabi-size build/cubeide.elf
   text    data     bss     dec     hex filename
   1140      12    1564    2716     a9c build/cubeide.elf
arm-none-eabi-objcopy -O ihex build/cubeide.elf build/cubeide.hex
arm-none-eabi-objcopy -O binary -S build/cubeide.elf build/cubeide.bin
```

- on lance openOCD en lui passant notre config stlinkV2, avec la commande "init" :`openocd -f openocd/stlinkv2_mini.cfg -c "init"`
  - on doit voir l'ouverture du port 3333 qui écoute une éventuelle  connexion de gdb
  - sortie : 
```
╰─ openocd -f ../stlinkv2_mini.cfg  -c "init"
Open On-Chip Debugger 0.11.0-g228ede4 (2022-09-26-00:59)
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
Info : The selected transport took over low-level target control. The results might differ compared to plain JTAG/SWD
none separate

Info : clock speed 1000 kHz
Info : STLINK V2J29S7 (API v2) VID:PID 0483:3748
Info : Target voltage: 3.240399
Info : stm32f1x.cpu: hardware has 6 breakpoints, 4 watchpoints
Info : starting gdb server for stm32f1x.cpu on 3333
Info : Listening on port 3333 for gdb connections
Info : Listening on port 6666 for tcl connections
Info : Listening on port 4444 for telnet connections

```
- dans un autre terminal, au meme endroit,on lance gdb sur le programme compiléé: `arm-none eabi-gdb .build/cubeide.elf`
  - sortie :
```
╰─ arm-none-eabi-gdb build/cubeide.elf 
GNU gdb (Ubuntu 12.0.90-0ubuntu1) 12.0.90
Copyright (C) 2022 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Type "show copying" and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<https://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.

For help, type "help".
Type "apropos word" to search for commands related to "word"...
GEF for linux ready, type `gef' to start, `gef config' to configure
96 commands loaded for GDB 12.0.90 using Python engine 3.10
Reading symbols from build/cubeide.elf...
gef➤  

```
  - dans gdb : `target remote localhost:3333`
    - sortie :
```
gef➤  target remote localhost:3333
Remote debugging using localhost:3333
main () at app/src/main.c:25
25                      for (k=0;k<255;k++);
[ Legend: Modified register | Code | Heap | Stack | String ]
──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── registers ────
$r0  : 0x00000017  →  0x0001e508  →   ; <UNDEFINED> instruction: 0xffffffff
$r1  : 0x00000004  →  0x08000401  →  0x0d490d48  →  0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
$r2  : 0x000000e9  →  0x00080004  →  0x08000401  →  0x0d490d48  →  0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000
$r3  : 0x000000e7  →  0x00044908  →   ; <UNDEFINED> instruction: 0xffffffff
$r4  : 0x20000020  →  0x25907360  →  0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
$r5  : 0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
$r6  : 0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
$r7  : 0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
$r8  : 0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
$r9  : 0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
$r10 : 0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
$r11 : 0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
$r12 : 0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
$sp  : 0x20004ff8  →  0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
$lr  : 0x080003e3  →  0x01e00821  →  0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
$pc  : 0x080003e6  →  0x2bfe3301  →  0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── stack ────
0x20004ff8│+0x0000: 0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]  ← $sp
0x20004ffc│+0x0004: 0x08000433  →  0x00000047  →  0x00044908  →   ; <UNDEFINED> instruction: 0xffffffff
0x20005000│+0x0008: 0xb88ec230  →  0x00000000  →  0x20005000  →  [loop detected]
0x20005004│+0x000c: 0x7a1694ec  →  0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
0x20005008│+0x0010: 0x900e3d18  →  0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
0x2000500c│+0x0014: 0x72e43feb  →  0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
0x20005010│+0x0018: 0xa1aef22b  →  0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
0x20005014│+0x001c: 0xefa4fdbe  →  0x00000000  →  0x20005000  →  0xb88ec230  →  0x00000000  →  [loop detected]
[!] Command 'context' failed to execute properly, reason: unsupported operand type(s) for &: 'NoneType' and 'int'
gef➤  

```
  - `monitor targets` pour voir des infos sur la cible (la carte blue  pill connectée.) 
    - sortie : 
```
gef➤  monitor targets
    TargetName         Type       Endian TapName            State       
--  ------------------ ---------- ------ ------------------ ------------
 0* stm32f1x.cpu       hla_target little stm32f1x.cpu       halted

gef➤  
```
  - `monitor stm32f1x mass_erase 0` pour vider la memoire de la stm32 (supprime le programme éventuellement présent) :
```
gef➤  monitor stm32f1x mass_erase 0
stm32x mass erase complete

gef➤  
```

- `monitor flash write_bank 0 /xxxxx/devel/template_bluepill/build/cubeide.bin`  pour uploader le programe (.bin) sur la stm32 (chemin absolu necessaire):
  - sortie :
```
gef➤  monitor flash write_bank 0 /xxxxx/devel/template_bluepill/build/cubeide.bin
wrote 1152 bytes from file /xxxxx/devel/template_bluepill/build/cubeide.bin to flash bank 0 at offset 0x00000000 in 0.157229s (7.155 KiB/s)

gef➤ 
```

- La diode ne clignote pas, le programme est "halt". Pour le lancer : `monitor reset run`
  - sortie (la led verte de la blue pill clignote rapidement): 
```
gef➤  monitor reset run
gef➤ 
```


##  Explication de code

[A suivre...]  : https://www.twitch.tv/videos/1604674037?t=29m
----

![fin](images/thats_all_folks_porky.png)











